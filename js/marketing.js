
/*多、精、省、好 插件*/
!function(a) {
    var b = function() {
        this.init()
    };
    b.prototype = {
        init: function() {
            this.productShow()
        },
        productShow: function() {
            var b = a(".J_productsBox"),
            c = b.find("li.J_item"),
            d = function() {
                var b = a(window).width();
                return 1200 > b ? !0 : !1
            };
            c.hover(function() {
                var b = a(this),
                e = d() ? 200 : 200,
                f = d() ? 500 : 500;
                c.not(b).stop().animate({
                    width: e
                }).removeClass("on"),
                b.stop().animate({
                    width: f
                }).addClass("on")
            }),
            a(window).resize(function() {
                c.filter(".on").mouseenter()
            })
        }
    },
    new b
} (jQuery);
/*多、精、省、好 插件 end*/
